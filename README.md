JAWS 100
Features:
1. The game is endless
2. Increasing levels increases the speed of moving obstacles

Assumptions in the game:
1. Both players need to use the same control
2. Score is given level wise only, overall score is not considered
3. Only level by level winners are declared
4. User is expected not to go out of the horizontal boundaries as it is counter productive